//
//  GroceryCell.swift
//  ShoppingList
//
//  Created by Haskel Ash on 5/26/16.
//  Copyright © 2016 Haskel Ash. All rights reserved.
//

import Foundation
import UIKit

class GroceryCell: UITableViewCell {

    let oddColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    let evenColor = UIColor(red: 0.952, green: 0.952, blue: 0.952, alpha: 1.0)

    var indexPath: NSIndexPath! {
        didSet {
            self.backgroundColor = self.indexPath.row % 2 == 0 ? oddColor : evenColor
        }
    }

    var grocery: Grocery! {
        didSet {
            self.checkmarkButton.selected = grocery.completed
            self.nameLabel.text = grocery.name
            self.detailsLabel.text = grocery.details
            self.quantityLabel.text = String(grocery.quantity)
        }
    }

    @IBOutlet weak private var checkmarkButton: UIButton!
    @IBOutlet weak private var nameLabel: UILabel!
    @IBOutlet weak private var detailsLabel: UILabel!
    @IBOutlet weak private var quantityLabel: UILabel!

    @IBAction func toggleCompleted(button: UIButton) {
        //toggle the button and the grocery's field
        button.selected = !button.selected
        self.grocery.completed = button.selected

        //save it
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        do {
            try appDelegate.managedObjectContext.save()
        } catch let error as NSError {
            print(error.description)
        }
    }
}
