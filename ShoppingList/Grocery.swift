//
//  Grocery.swift
//  ShoppingList
//
//  Created by Haskel Ash on 5/25/16.
//  Copyright © 2016 Haskel Ash. All rights reserved.
//

import Foundation
import CoreData


class Grocery: NSManagedObject {

    override func awakeFromInsert() {
        self.creationTime = NSDate()
    }

    class func entityName() -> String {
        return "Grocery"
    }

    class func creationTimeKey() -> String {
        return "creationTime"
    }
}
