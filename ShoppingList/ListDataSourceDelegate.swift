//
//  ListDataSourceDelegate.swift
//  ShoppingList
//
//  Created by Haskel Ash on 5/25/16.
//  Copyright © 2016 Haskel Ash. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class ListDataSourceDelegate: NSObject, UITableViewDataSource, UITableViewDelegate {

    var fetchDelegate: NSFetchedResultsControllerDelegate? {
        willSet {
            self.fetchedResultsController.delegate = newValue
        }
    }

    private let fetchedResultsController: NSFetchedResultsController!
    private let groceryIdentifier = "GroceryCell"
    private let noDataIdentifier = "NoDataCell"

    override init() {

        //set up our fetched request controller
        let request = NSFetchRequest(entityName: Grocery.entityName())
        request.sortDescriptors = [NSSortDescriptor(key: Grocery.creationTimeKey(), ascending: true)]
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

        self.fetchedResultsController = NSFetchedResultsController(fetchRequest: request,
                                                                   managedObjectContext: appDelegate.managedObjectContext,
                                                                   sectionNameKeyPath: nil,
                                                                   cacheName: nil)

        //perform the fetch
        do {
            try self.fetchedResultsController?.performFetch()
        } catch let error as NSError {
            print(error.description)
        }

        super.init()
    }

    //table view data source methods

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        let sections = self.fetchedResultsController.sections?.count ?? 1
        return sections > 0 ? sections : 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let rows = self.fetchedResultsController.sections?[section].numberOfObjects ?? 1
        return rows > 0 ? rows : 1
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //if not data, disable scrolling and short circuit return the no data cell
        if self.fetchedResultsController.sections?[indexPath.section].numberOfObjects == 0{
            tableView.scrollEnabled = false
            return tableView.dequeueReusableCellWithIdentifier(noDataIdentifier, forIndexPath: indexPath)
        }

        //enable scrolling
        tableView.scrollEnabled = true

        //get the grocery at the given index path and pass it to the cell
        let cell = tableView.dequeueReusableCellWithIdentifier(
            groceryIdentifier, forIndexPath: indexPath) as! GroceryCell
        cell.grocery = self.fetchedResultsController.objectAtIndexPath(indexPath) as! Grocery

        //tell the cell what index path it is
        cell.indexPath = indexPath

        return cell
    }

    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle,
                   forRowAtIndexPath indexPath: NSIndexPath) {

        //delete the object at the given index path
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.managedObjectContext.deleteObject(
            self.fetchedResultsController.objectAtIndexPath(indexPath) as! Grocery)

        //save it
        do {
            try appDelegate.managedObjectContext.save()
        } catch let error as NSError {
            print(error.description)
        }

        if tableView.numberOfRowsInSection(0) == 1 {
            //deleting the first and only cell, reload it as prompt cell instead of deleting
            tableView .reloadRowsAtIndexPaths([indexPath], withRowAnimation: .None)
        } else {
            //delete the corresponding row
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .None)

            //reload all rows (because of odd/even coloring)
            tableView.reloadData()
        }
    }

    //table view delegate methods

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if self.fetchedResultsController.sections?[indexPath.section].numberOfObjects == 0 {
            return tableView.frame.size.height
        } else {
            return 44
        }
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //deselect the row
        tableView.deselectRowAtIndexPath(indexPath, animated: true)

        //fire off notification for the view controller to pick up
        //so that it knows which grocery was selected
        let selectedGrocery = self.fetchedResultsController.objectAtIndexPath(indexPath) as! Grocery
        NSNotificationCenter.defaultCenter().postNotificationName(
            kDidSelectGroceryNotification,
            object: self,
            userInfo: [kGroceryKey: selectedGrocery])
    }
}
