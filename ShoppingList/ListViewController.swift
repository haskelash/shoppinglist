//
//  ListViewController.swift
//  ShoppingList
//
//  Created by Haskel Ash on 5/25/16.
//  Copyright © 2016 Haskel Ash. All rights reserved.
//

import UIKit
import CoreData

class ListViewController: UIViewController, NSFetchedResultsControllerDelegate {

    @IBOutlet weak private var tableView: UITableView!
    private let listDataSourceDelegate: ListDataSourceDelegate = ListDataSourceDelegate()

    override func viewDidLoad() {
        super.viewDidLoad()

        //set our table view's data source and delegate
        self.tableView.dataSource = self.listDataSourceDelegate
        self.tableView.delegate = self.listDataSourceDelegate

        //set ourselves as the fetch results controller's delegate
        self.listDataSourceDelegate.fetchDelegate = self

        //set up the offset of the table view relative to the bottom button
        self.tableView.contentInset.bottom = 44

        //subscirbe to notifications
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: #selector(didSelectGroceryNotification(_:)),
            name: kDidSelectGroceryNotification,
            object: self.tableView.delegate)
    }

    // fetched results controller delegate methods

    func controller(controller: NSFetchedResultsController
        , didChangeObject anObject: AnyObject
        , atIndexPath indexPath: NSIndexPath?
        , forChangeType type: NSFetchedResultsChangeType
        , newIndexPath: NSIndexPath?) {

        if type == .Insert, let path = newIndexPath {
            if path.row == 0 {
                //if this is first cell being added, we need to reload the prompt cell which is already at index 0
                self.tableView.reloadRowsAtIndexPaths([path], withRowAnimation: .Top)
            } else {
                //insert the new row
                self.tableView.insertRowsAtIndexPaths([path], withRowAnimation: .Top)
            }
        } else if type == .Update, let path = indexPath {
            self.tableView.reloadRowsAtIndexPaths([path], withRowAnimation: .None)
        }
    }

    // notification response methods

    func didSelectGroceryNotification(notification: NSNotification) {
        if let selectedGrocery = notification.userInfo?[kGroceryKey] as? Grocery {
            let editGroceryVC = self.storyboard?
                .instantiateViewControllerWithIdentifier(
                    kAddEditGroceryViewControllerIdentifier) as! AddEditGroceryViewController
            editGroceryVC.grocery = selectedGrocery
            self.presentViewController(editGroceryVC, animated: true, completion: nil)
        }
    }
}
