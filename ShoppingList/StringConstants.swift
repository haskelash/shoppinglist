//
//  StringConstants.swift
//  ShoppingList
//
//  Created by Haskel Ash on 5/26/16.
//  Copyright © 2016 Haskel Ash. All rights reserved.
//

import Foundation

//notification constants
let kDidSelectGroceryNotification = "DidSelectGroceryNotification"
let kGroceryKey = "grocery"

//storyboard constants
let kAddEditGroceryViewControllerIdentifier = "AddEditGroceryViewController"