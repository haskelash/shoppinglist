//
//  AddEditGroceryViewController.swift
//  ShoppingList
//
//  Created by Haskel Ash on 5/25/16.
//  Copyright © 2016 Haskel Ash. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class AddEditGroceryViewController: UIViewController, UITextViewDelegate {

    var grocery: Grocery?
    private var hasPlaceholderText = true
    @IBOutlet weak private var saveButton: UIButton!
    @IBOutlet weak private var nameTextField: UITextField!
    @IBOutlet weak private var detailsTextView: UITextView!
    @IBOutlet weak private var quantityStepper: UIStepper!
    @IBOutlet weak private var quantityLabel: UILabel!

    override func viewDidLoad() {
        //update fields if editing exisitng grocery
        if let existingGrocery = self.grocery {
            self.saveButton.enabled = true
            self.nameTextField.text = existingGrocery.name
            if existingGrocery.details?.characters.count > 0 {
                self.detailsTextView.text = existingGrocery.details
                self.detailsTextView.textColor = UIColor.blackColor()
                self.hasPlaceholderText = false
            }
            self.quantityStepper.value = Double(existingGrocery.quantity)
            self.quantityLabel.text = String(existingGrocery.quantity)
        }

        //add target action for text field change
        self.nameTextField.addTarget(self
            , action: #selector(textFieldEditingChanged(_:))
            , forControlEvents: .EditingChanged)

        //add target action for quantity stepper
        self.quantityStepper.addTarget(self
            , action: #selector(stepperValueChanged(_:))
            , forControlEvents: .ValueChanged)

        //subcribe to keyboard notifications
        NSNotificationCenter.defaultCenter().addObserver(
            self, selector: #selector(keyboardWillShowNotification(_:)),
            name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(
            self, selector: #selector(keyboardWillHideNotification(_:)),
            name: UIKeyboardWillHideNotification, object: nil)

        //show keyboard right away
        self.nameTextField.becomeFirstResponder()
    }

    //notification methods

    func keyboardWillShowNotification(notification: NSNotification) {
        if let keyboardFrame = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            for constraint in self.view.constraints {
                if constraint.secondItem === self.detailsTextView
                && constraint.secondAttribute == .Bottom {
                    constraint.constant = 20 + keyboardFrame.CGRectValue().size.height
                    break
                }
            }
        }
    }

    func keyboardWillHideNotification(notification: NSNotification) {
        for constraint in self.view.constraints {
            if constraint.secondItem === self.detailsTextView
                && constraint.secondAttribute == .Bottom {
                constraint.constant = 20
                break
            }
        }
    }

    //actions

    func textFieldEditingChanged(textField: UITextField) {
        self.saveButton.enabled = textField.text?.characters.count > 0
    }

    func stepperValueChanged(stepper: UIStepper) {
        self.quantityLabel.text = String(Int(stepper.value))
    }

    @IBAction func cancelTapped(button: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func doneTapped(button: UIButton) {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

        //if we have a grocery use it, if not then create a new one
        let grocery = self.grocery ?? {
            //create a new Grocery
            NSEntityDescription.insertNewObjectForEntityForName(
                Grocery.entityName(),
                inManagedObjectContext: appDelegate.managedObjectContext) as! Grocery
        }()

        //update fields
        grocery.name = self.nameTextField.text!
        if (!self.hasPlaceholderText) {
            grocery.details = self.detailsTextView.text
        }
        grocery.quantity = Int16(self.quantityStepper.value)

        //save it
        do {
            try appDelegate.managedObjectContext.save()
        } catch let error as NSError {
            print(error.description)
        }

        //dismiss view controller
        self.cancelTapped(button)
    }

    //text view delegate methods

    func textViewDidBeginEditing(textView: UITextView) {
        if self.hasPlaceholderText {
            self.detailsTextView.text = nil
            self.detailsTextView.textColor = UIColor.blackColor()
            self.hasPlaceholderText = false
        }
    }

    func textViewDidEndEditing(textView: UITextView) {
        if self.detailsTextView.text.characters.count == 0 {
            self.detailsTextView.text = "Details ..."
            self.detailsTextView.textColor = UIColor(red: 0.784, green: 0.784, blue: 0.784, alpha: 1.00)
            self.hasPlaceholderText = true
        }
    }
}