# Shopping List #
A shopping list application for iOS devices
### App features ###

* View all of your groceries in a single list
* Check off groceries that you have purchased
* Tap an item on your list for more details
* Add new items to your list
* Add the name of a new item
* Add a short description for each item
* Edit the quantity of the item
* Delete an item

### How do I get set up? ###

* Download the repository from Bitbucket
* Open ShoppingList.xcodeproj to open the project in Xcode
* Select the device to run on from the dropdown at the top
* Click the play button